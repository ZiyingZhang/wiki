# Batch Analysis

GenomeR allows users to upload multiple k-mer profiles for 
analysis. Select "Batch Analysis" on the tabs and upload the files 
using the "Input Settings". You will have to select all you files 
and upload them at once so make sure you have all your files under 
the same directory.

After uploading your files, two tables will be displayed, one is 
the size predictions summary of all the files and the other is the 
GenomeScope statistics of all the files. You can adjust the 
parameters and the tables will be updated accordingly. You can also
download the tables into csv formats.

![](batch_example.png)


[Home](Home.md)
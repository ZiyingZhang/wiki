# Tutorial

[TOC]

## Running Genomer

To run Genomer, simply run the following command in an R session 
(replace `<GENOMER_DIR>` with your own directory of GenomeR):

```
shiny::runApp('<GENOMER_DIR>')
```

GenomeR should then be available in a browser window. Continue with 
the following sections to get more details about the usage of 
GenomeR.

## Types of Inputs in GenomeR

GenomeR supports 3 different types of inputs. File Input, 
Sample Data and Simulation Data.

### File Input

GenomeR accepts a k-mer distribution histogram. Select the 
"File upload" option on the Input Settings panel.

You can use tools such as 
[Jellyfish](http://www.genome.umd.edu/jellyfish.html) to compute 
the histogram from your sequencing data. Below is an example 
command of running Jellyfish:

```
jellyfish count -m 21 -s 100M -t 10 -C reads.fasta && jellyfish histo -t 10 reads.jf > reads.histo
```

For more instructions on Jellyfish, please refer to their 
[documentation](https://github.com/gmarcais/Jellyfish/blob/master/README.md).

### Sample Data

GenomeR provides a set of sample data for users to find out the 
functionality of GenomeR. Select the "Sample data" option on the 
Input Settings panel and then choose a sample k-mer 
profile from the drop down under "Sample Selection"

### Simulation Data

GenomeR allows simulating the k-mer profile which can be used by 
selecting "Simulation data" on the Input Settings panel.

Parameters for the simulation data can be changed to change the 
data. These parameters include:

* Genome Size
* Sequencing Coverage
* Read Legnth
* K-mer Length
* Data Cutoff
* Error Rate
* Ploidy Type
* Heterozygosity

## Model Settings

GenomeR consists different settings for different models. You can 
set the parameters before uploading a k-mer profile. You can also 
adjust the parameters after a profile is uploaded and the outputs 
will be updated accordingly to provide an interactive experience.
To change more GenomeScope settings select "Advanced Settings" to
show settings such as Error-cutoff.

![](mode_settings_example.gif)

## GenomeR Outputs

### Output Models

GenomeR plots the model under "Output Model" and you can pick the 
model that you want to see. You can also interact with the plots, 
for example: 

* Clicking the legend can show or hide different parts of the 
model
* Putting your cursor close to a point will display the data of 
the closest point to the cursor
* You can zoom in or out to visualise different parts of the model

### Size Predictions

A size prediction table will also be displayed on the right which 
summarises the size predictions of all the models.

There will also be an extra table when GenomeScope is chosen as the
output model. This table summarises some of the statistics of 
GenomeScope for its predictions.

## What to read next?

* You can perform [Results Analysis](Results.md) afterwards

* You can [download](Download.md) your results

* If you want to analysis multiple k-mer profiles, have a look at
[Batch Analysis](Batch.md)

[Home](Home.md)
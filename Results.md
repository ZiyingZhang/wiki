# Results Analysis

After uploading your k-mer profile and adjusting the parameters, you
can go to the Results tab to obtain a comparison of the estimation
by the three models. Simply click "Generate Report" to perform the
analysis. 

An interactive plot will be generated comparing the estimated sizes
by the three models with different cutoffs. You can also see the 
actual values from the generated table.

You can also press "Download Report' to save your analysis.

![](results_example.png)

# Welcome to GenomeR Wiki

* What is GenomeR? See [Introduction](Introduction.md)
* If this is your first time running Genomer, follow the 
instructions in [Installing Dependencies](Installation.md)
* To start and run GenomeR, read the [Tutorial](Tutorial.md)

## Table of Contents
* [Home](Home.md)
* [Installing Dependencies](Installation.md)
* [Introduction to Genomer](Introduction.md)
* [Tutorial](Tutorial.md)
* [Results Analysis](Results.md)
* [Batch Analysis](Batch.md)
# Installing Dependencies

We have included a R script which will install all the dependencies 
for GenomeR automatically. Simply go to directory of the cloned 
repository and run the following command:
 
```
Rscript requirements.R
```
 
You can also run the following command in an R session to install 
the dependencies:

```
install.packages(c("shiny", "shinyjs", "shinyWidgets", "shinycssloaders", "plotly", "quantmod", "tidyverse"))
```

## What to read next?

* Read [Tutorial](Tutorial.md) to start and run GenomeR

[Home](Home.md)
#Downloading Results

GenomeR allows you to download the results of the k-mer size estimation
in several formats.

##Report

A final report is generated on the [Results](Results.md) page that includes
all inputs and outputs produced by GenomeR for a single selected file. 
This includes Peak Frequency, Simple Count and GenomeScope plots.
 

##Size Predictions

A csv file of the size predictions produced by all three methods can be
downloaded by pressing the "Download Table" button directly below 
the size estimation table on the output page. This file includes 
input parameters:

* K-mer length

* Read length

* Error cutoff

* K-mer cutoff



##GenomeScope output

A csv file of GenomeScope outputs can also be produced by pressing the 
"Download Table" button directly below GenomeScope Statistics.
This file all the information in the table. 
This table is only present on the output page when GenomeScope
is selected as the 

##Batch Results

GenomeR can also perform [Batch Analysis](Batch.md) and a csv of size estimations 
for all files can be downloaded by selecting "Download size prediction as csv" button. 
A table of GenomeScope statistics can also be downloaded in csv format by 
selecting the "Download GenomeScope statistics as csv" button.

[Home](Home.md)